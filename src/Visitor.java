
public interface Visitor {
	
	public void Visit(Visitable v);
	
}
