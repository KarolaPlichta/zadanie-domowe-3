
public class Product implements Visitable {
	
	private String name;
	private double price;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void accept(Visitor visitor) {
		visitor.Visit(this);
	}
	
	public int giveReport() {
		return 0;
	}
	

}
