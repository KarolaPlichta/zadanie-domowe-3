
public interface Visitable {

	public void accept(Visitor visitor);
	public int giveReport();
}
