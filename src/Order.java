import java.util.Date;
import java.util.ArrayList;


public class Order implements Visitable {
	
	private String number;
	private double orderTotalPrice;
	private Date orderDate;
	private ArrayList<Product> products;
	
	public Order(String number){
		this.number = number;
		this.products = new ArrayList<Product>();
	}
	
	public void addProduct(Product product){
		this.products.add(product);
	}
	
	public String getNumber(){
		return number;
	}
	
	public double getOrderTotalPrice(){
		return orderTotalPrice;
	}
	
	public Date getOrderDate(){
		return orderDate;
	}

	
	public void accept(Visitor visitor) {
		visitor.Visit(this);
		
	}

	
	public int giveReport() {
		return this.products.size();
	}
	

}
