import java.util.ArrayList;


public class Client implements Visitable {
	
	private String number;
	private ArrayList<Order> orders;
	
	public Client(String number){
		this.number = number;
		this.orders = new ArrayList<Order>();
	}
	
	public void addOrder(Order order){
		this.orders.add(order);
	}


	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}


	public void accept(Visitor visitor) {
		visitor.Visit(this);
		
	}


	public int giveReport() {
		return this.orders.size();
	}

}
