import java.util.ArrayList;


public class ClientGroup implements Visitable{
	
	private String memberName;
	private ArrayList<Client> clients;
	
	public ClientGroup(String memberName){
		this.memberName = memberName;
		this.clients = new ArrayList<Client>();
	}
	
	public void addClient(Client client){
		this.clients.add(client);
	}

	public void accept(Visitor visitor) {
		visitor.Visit(this);
		
	}

	public int giveReport() {
		return clients.size();
	}

}
